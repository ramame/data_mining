setwd("D:/ram/?????? ??/?????????? ??/data mining words" )

weather.original <- read.csv('weatherAUS.csv')
weather.prepared <- weather.original

#logistic regression
summary(weather.prepared)
str(weather.prepared)

#Feature Extraction + EDA.

date <- weather.prepared$Date
date.length <- sapply(as.character(date), nchar)
check.length <- date.length[date.length == 10]

#all dates have same structure
#separate month and year to two fetures

weather.prepared$month <- substr(as.character(weather.original$Date),6,7) 
weather.prepared$year <-  as.numeric(substr(as.character(weather.original$Date),1,4))
weather.prepared$Date <- NULL


#libary for EDA
install.packages('ggplot2')
library(ggplot2)
ggplot(weather.prepared,aes(month)) + geom_bar(aes(fill = RainTomorrow))
#its deppends on how many obs in our data then we can use other plot for results in % :
ggplot(weather.prepared) + aes(x = month, fill = RainTomorrow) +geom_bar(position = "fill")
#we saw big diffrence in month 6,7,8,9
#month to be factor that show high rains day as winter
seasons <- c('Not_winter', 'Winter', 'Not_winter')
breaks <- c(0,5,9,12)
bins <- cut(as.numeric(weather.prepared$month) , breaks = breaks, labels = seasons, include.lowest = F, right = T)
weather.prepared$season <- bins
ggplot(weather.prepared) + aes(x = season, fill = RainTomorrow) +geom_bar(position = "fill")
#thats look better but i really cant know without testing one model with month and one model with season 
#and see what better (by accuracy, precsion and recall)
str(weather.prepared)
weather.prepared$month <- NULL
ggplot(weather.prepared) + aes(x = Location, fill = RainTomorrow) +geom_bar(position = "fill")
#location is fine really important.

#minimum and maximum temerture
ggplot(weather.prepared, aes(MinTemp,MaxTemp)) + geom_bin2d(aes(fill = RainTomorrow), binwidth = c(2,2))
#what to do with it? 

ggplot(weather.original, aes(Location,MinTemp)) + geom_boxplot()
ggplot(weather.original, aes(Location,MaxTemp)) + geom_boxplot()


deal_NA_Max_Temp <- function(x, location){ 
  
  mean.location <- mean(weather.original$MaxTemp[weather.original$Location == location], na.rm = TRUE)  
  if(is.na(x)){
    return(mean.location)
  }
  else{
    return(x)
  }
}
  
deal_NA_Min_Temp <- function(x, location){
  mean.location <- mean(weather.original$MinTemp[weather.original$Location == location], na.rm = TRUE)  
  if(is.na(x)){
    return(mean.location)
  }
  else{
    return(x)
  }
}

weather.prepared$MinTemp <- mapply(deal_NA_Min_Temp, x = weather.original$MinTemp, location = as.character(weather.original$Location))
weather.prepared$MaxTemp <- mapply(deal_NA_Max_Temp, x = weather.original$MaxTemp, location = as.character(weather.original$Location))
summary(weather.original$MaxTemp)
summary(weather.prepared$MaxTemp)

#lazy option for make na not null
make_average <- function(x,xvec){
  if(is.na(x)){
    return (mean(xvec, na.rm=TRUE))
  }()
  return (x)
}
make_unknown <- function(x){
  if(is.na(x)){
    return('unknown')
  }
  else{
    return(x)
  }
}
summary(weather.prepared)
summary(weather.original)
#function not work
weather.prepared$Rainfall <- sapply(weather.prepared$Rainfall,make_average, xvec = weather.prepared$Rainfall)
weather.prepared$Evaporation <- sapply(weather.prepared$Evaporation,make_average, xvec = weather.prepared$Evaporation)
weather.prepared$Sunshine <- sapply(weather.prepared$Sunshine,make_average, xvec = weather.prepared$Sunshine)
#choose not to analayze (lazy)
weather.prepared$WindDir9am <- NULL
weather.prepared$WindDir9am <- NULL
weather.prepared$Humidity9am <- NULL
weather.prepared$WindGustSpeed <- sapply(weather.prepared$WindGustSpeed,make_average, xvec = weather.prepared$WindGustSpeed)
#see what factor is important
ggplot(weather.prepared) + aes(x = WindGustDir, fill = RainTomorrow) +geom_bar(position = "fill")
ggplot(weather.prepared) + aes(x = RainToday, fill = RainTomorrow) +geom_bar(position = "fill")
weather.prepared$WindGustDir <- as.factor(sapply(weather.original$WindGustDir, make_unknown))
weather.prepared$RainToday <- as.factor(sapply(weather.original$RainToday, make_unknown))
summary(weather.prepared)
weather.prepared$WindDir3pm <- NULL
weather.prepared$WindSpeed3pm <- NULL
weather.prepared$WindSpeed3pm <- NULL
weather.prepared$WindSpeed9am <- NULL
weather.prepared$Humidity3pm <- NULL
weather.prepared$Pressure3pm <- NULL
weather.prepared$Pressure9am <- NULL
weather.prepared$Cloud9am <- NULL
weather.prepared$Cloud3pm <- sapply(weather.prepared$Cloud3pm,make_average, xvec = weather.prepared$Cloud3pm)
weather.prepared$Temp9am <- NULL
weather.prepared$Temp3pm <- sapply(weather.prepared$Temp3pm,make_average, xvec = weather.prepared$Temp3pm)
summary(weather.prepared)
#building the model
weather.prepared$RainTomorrow <- sapply(weather.original$RainTomorrow,convert_zero_one)
install.packages('caTools')
library(caTools)
set.seed(101)
filter <- sample.split(weather.prepared$RISK_MM , SplitRatio = 0.7)

weather.train <- subset(weather.prepared, filter ==T)
weather.test <- subset(weather.prepared, filter ==F)
summary(weather.prepared)
dim(weather.train)
weather.model <- glm(RainTomorrow ~ ., family = binomial(link = 'logit'), data = weather.train)

step.model.income <- step(weather.model)
summary(step.model.income)

predicted.weather.test <- predict(weather.model, newdata = weather.test, type = 'response')

confusion_matrix <- table(weather.test$RainTomorrow, predicted.weather.test > 0.8 )
str(weather.test)

class(confusion_matrix)

recall <- confusion_matrix[1,1]/(confusion_matrix[1,1] + confusion_matrix[1,2])
precision <- confusion_matrix[1,1]/(confusion_matrix[1,1] + confusion_matrix[2,1])

print(confusion_matrix)
print(recall)
print(precision)

convert_zero_one <- function(Rain){
  if(Rain == 'Yes'){
    return(1)
  }
  else{
    return(0)
  }
}
