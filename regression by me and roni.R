setwd("D:/ram/��� �/����� �/data mining words/housesalesprediction")

house.original <- read.csv("kc_house_data.csv")
house <- house.original

install.packages("ggplot2")
library(ggplot2)

head(house)
summary(house)
str(house)

#histogram
ggplot(house, aes(price)) + geom_histogram(binwidth = 50000)


house$id <- NULL

house$date <- as.character(house$date)
house$year <- substr(house$date,1,4)


ggplot(house, aes(as.factor(year),price)) + ylim(0,2000000)+ geom_boxplot() 
#not effected

#how bedrooms affects prices 
ggplot(house, aes(bedrooms)) + geom_histogram(binwidth = 0.5)
ggplot(house, aes(bedrooms,price)) + geom_point() + stat_smooth(method = lm)
ggplot(house, aes(as.factor(bedrooms),price))) + geom_boxplot() 



#how sqft_living affects prices 
ggplot(house, aes(sqft_living)) + geom_histogram()
ggplot(house, aes(as.factor(sqft_living),price)) + geom_boxplot()
ggplot(house, aes(sqft_living,price)) + geom_point() + stat_smooth(method = lm)

ggplot(house, aes(as.factor(view),price)) + geom_boxplot()



#Anlize zipcode distribution (can we extract somethng from it?)
ggplot(house, aes(zipcode)) + geom_histogram(binwidth = 1)
zip <- as.character(house$zipcode)
zip1  <- substr(zip,3,4)
house$zip1 <- zip1
house$zip1 <- as.factor(house$zip1)
ggplot(house, aes(as.factor(zip1),price)) + geom_boxplot()


ggplot(house, aes(lat,price)) + geom_point() + geom_smooth(method = 'lm')
#(Looks like that going north makes houses more costly)


ggplot(house, aes(yr_built,price)) + geom_point() + geom_smooth(method = 'lm')

house$yr_built <- house.original$yr_renovated[house.original$yr_renovated == 0]

summary(house)


revont <- function(x){
  if(x == 0){
    return("not revonted")
  }
  else{
    return("revonted")
  }
  
}


house$yr_renovated <- sapply(house.original$yr_renovated, revont)

ggplot(house, aes(as.factor(yr_renovatoed),price)) + geom_boxplot()
summary(house)
house$id <- NULL
house$date <- NULL 
house$zipcode <- NULL
house$lat <- NULL 
house$yr_built <- NULL 

names <- c("view not to water", "view to water")
house$waterfront <- factor(house$waterfront, levels = 0:1 , labels = names )

ggplot(house, aes(house$waterfront,price)) + geom_boxplot()




#divide into trainig set and test set 
library(caTools)

filter <- sample.split(house, SplitRatio = 0.7)

real.train <- subset(house, filter ==T)
real.test <- subset(house, filter ==F)


#applying the linear regression model 
model <- lm(price ~ ., real.train)

summary(model)
#looks like most features are meaningfull 
predicted.train <- predict(model,real.train)
predicted.test <- predict(model,real.test)


MSE.train <- mean((real.train$price - predicted.train)**2)
MSE.test <- mean((real.test$price - predicted.test)**2)

RMSE.train <- MSE.train**0.5 #209655
RMSE.test <- MSE.test**0.5 #211315
#there is a very slight overfitting 


#what is the mean price
mprice <- mean(house$price) # 540088

#error in percantage of the mean
per_error <-RMSE.test/mprice #40% prety large error 



























